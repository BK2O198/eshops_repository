const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const logger = require("./middleware/logger");
const client = require("./routes/api/mongo-db").client;
const run = require("./routes/api/mongo-db").run;
const random_name = require("random-name");
const Fakerator = require("fakerator");
const fakerator = Fakerator("en-CA");
var _ = require("lodash");

var deleteDatabase = async function (client) {
  client.connect();
  const database = client.db("EShops");
  database.dropDatabase(function (err, result) {
    console.log("Error : " + err);
    if (err) throw err;
    console.log("Operation Success ? " + result);
  });

  return;
};

var getPhonesByShopId = async function (client, shopId) {
  client.connect();
  const database = client.db("EShops");
  const phones = database.collection("phones");
  const filter = { shopId: shopId };

  const searchCursor = await phones.find(filter);
  const result = await searchCursor.toArray();
  // print a message if no documents were found
  if ((await searchCursor.count()) === 0) {
    console.log("No documents found!");
  }
  console.log(result);

  return result;
};

var getUser = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik = await ulogovani_korisnik.find();
  const user = await korisnik.toArray();
  return user;
};

var getComputersByShopId = async function (client, shopId) {
  client.connect();
  const database = client.db("EShops");
  const computers = database.collection("computers");
  const filter = { shopId: shopId };
  const searchCursor = await computers.find(filter);
  const result = await searchCursor.toArray();
  if ((await searchCursor.count()) === 0) {
    console.log("No documents found!");
  }
  console.log(result);

  return result;
};

var getShopById = async function (client, shopId) {
  client.connect();
  console.log("IDD", shopId);
  const database = client.db("EShops");
  const shops = database.collection("shops");
  const filter = { shopId: shopId };
  const searchCursor = await shops.find(filter);
  const result = await searchCursor.toArray();
  if ((await searchCursor.count()) === 0) {
    console.log("No documents found!");
  }
  console.log(result);

  return result;
};

var getTvsByShopId = async function (client, shopId) {
  client.connect();
  const database = client.db("EShops");
  const tvs = database.collection("tvs");
  const filter = { shopId: shopId };
  const searchCursor = await tvs.find(filter);
  const result = await searchCursor.toArray();
  if ((await searchCursor.count()) === 0) {
    console.log("No documents found!");
  }
  console.log(result);

  return result;
};

var signout = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const user = database.collection("ulogovani_korisnik");
  const query = { userId: { $type: "string" } };
  const result = await user.deleteMany(query);
  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

var getUserById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const filter = { userId: id };
  const searchCursor = await users.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getTvById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const tvs = database.collection("tvs");
  const filter = { tvId: id };
  const searchCursor = await tvs.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getComputerById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const computers = database.collection("computers");
  const filter = { computerId: id };
  const searchCursor = await computers.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getPhoneById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const phones = database.collection("phones");
  const filter = { phoneId: id };
  const searchCursor = await phones.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var addToCartComputer = async function (client, computerId) {
  client.connect();
  const database = client.db("EShops");
  const computers = database.collection("computers");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik_id = await ulogovani_korisnik.find();
  const k_id = await korisnik_id.toArray();
  console.log(computerId);
  const filter = { computerId: computerId };
  const searchCursor = await computers.find(filter);
  const res = await searchCursor.toArray();
  console.log(res);
  const userId = k_id[0].userId;
  var cartItem = {
    userId: k_id[0].userId,
    shopId: res[0].shopId,
    productType: res[0].productType,
    type: res[0].type,
    manufacturer: res[0].manufacturer,
    price: res[0].price,
  };
  console.log("CART ITEM", cartItem);
  const carts = database.collection("carts");
  const doc = cartItem;
  const result = await carts.insertOne(doc);
  return userId;
};

var addToCartTv = async function (client, tvId) {
  client.connect();
  const database = client.db("EShops");
  const tvs = database.collection("tvs");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik_id = await ulogovani_korisnik.find();
  const k_id = await korisnik_id.toArray();
  console.log(tvId);
  const filter = { tvId: tvId };
  const searchCursor = await tvs.find(filter);
  const res = await searchCursor.toArray();
  console.log(res);
  const userId = k_id[0].userId;
  var cartItem = {
    userId: k_id[0].userId,
    shopId: res[0].shopId,
    productType: res[0].productType,
    type: res[0].type,
    manufacturer: res[0].manufacturer,
    price: res[0].price,
  };
  console.log("CART ITEM", cartItem);
  const carts = database.collection("carts");
  const doc = cartItem;
  const result = await carts.insertOne(doc);
  return userId;
};

var addToCartPhone = async function (client, phoneId) {
  client.connect();
  const database = client.db("EShops");
  const phones = database.collection("phones");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik_id = await ulogovani_korisnik.find();
  const k_id = await korisnik_id.toArray();
  console.log(phoneId);
  const filter = { phoneId: phoneId };
  const searchCursor = await phones.find(filter);
  const res = await searchCursor.toArray();
  console.log(res);
  const userId = k_id[0].userId;
  var cartItem = {
    userId: k_id[0].userId,
    shopId: res[0].shopId,
    productType: "Phone",
    type: res[0].type,
    manufacturer: res[0].manufacturer,
    price: res[0].price,
  };
  console.log("CART ITEM", cartItem);
  const carts = database.collection("carts");
  const doc = cartItem;
  const result = await carts.insertOne(doc);
  return userId;
};

var deleteUser = async function (client, id) {
  client.connect();
  console.log(id);
  const database = client.db("EShops");
  const users = database.collection("users");
  const logovanje_korisnika = database.collection("logovanje_korisnika");
  const query = { userId: id };
  const res = await users.find(query);
  const user = await res.toArray();
  console.log("User", user);
  const api_key = user[0].api_key;
  console.log("api_key", api_key);
  const query2 = { api_key: api_key };
  const result2 = await logovanje_korisnika.deleteMany(query2);
  const result = await users.deleteMany(query);

  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
  if (result2.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

var deletePhone = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const phones = database.collection("phones");
  const query = { phoneId: id };
  const result = await phones.deleteOne(query);
  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

var deleteComputer = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const computers = database.collection("computers");
  const query = { computerId: id };
  const result = await computers.deleteOne(query);
  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

var deleteTv = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const tvs = database.collection("tvs");
  const query = { tvId: id };
  const result = await tvs.deleteOne(query);
  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

var deleteShop = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const shops = database.collection("shops");
  const phones = database.collection("phones");
  const computers = database.collection("computers");
  const tvs = database.collection("tvs");
  const query = { shopId: id };
  const result = await shops.deleteOne(query);
  const result2 = await phones.deleteMany(query);
  const result3 = await computers.deleteMany(query);
  const result4 = await tvs.deleteMany(query);
  if (result.deletedCount === 1) {
    console.dir("Successfully deleted one document.");
  } else {
    console.log("No documents matched the query. Deleted 0 documents.");
  }
};

const app = express();

// Init middleware

// Handlebars Middleware
app.engine("handlebars", exphbs());
app.set("view engine", "handlebars");

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Homepage Route
app.get("/", async function (req, res, next) {
  getUser(client).then((user) => {
    console.log(user);
    if (user) {
      res.render("login", { alertType: "success", msg: "Default" });
    } else {
      res.render("index", { alertType: "success", msg: "Default" });
    }
  });
});

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

// Members API Routes
app.use("/api/users", require("./routes/api/users"));
app.use("/api/register", require("./routes/api/register"));
app.use("/api/create-update-shops", require("./routes/api/create-update-shops"));
app.use("/api/create-update-phones", require("./routes/api/create-update-phones"));
app.use("/api/create-update-computers", require("./routes/api/create-update-computers"));
app.use("/api/create-update-tvs", require("./routes/api/create-update-tvs"));
app.use("/api/login", require("./routes/api/login"));
app.use("/api/shops", require("./routes/api/shops"));
app.use("/api/phones", require("./routes/api/phones"));
app.use("/api/computers", require("./routes/api/computers"));
app.use("/api/tvs", require("./routes/api/tvs"));

app.get("/api/signout", (req, res) => {
  signout(client);
  res.render("login", { alertType: "success", msg: "Please login", logged: false, notlogged: true });
});

app.get("/api/phones/create/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  console.log(shopId);
  const tags = "apple iphone black orange white";
  var phone = {
    manufacturer: "Apple",
    type: "iPhone XR",
    productType: "Phone",
    subtype: "Smartphone",
    screenDiagonal: "6.1",
    ramMemory: 3,
    internalMemory: 128,
    camera: "212.0 Mpix",
    bateryCapacity: "2940 mAh",
    resolution: "1792 x 828",
    screenType: "IPS",
    price: "86.390 RSD",
    tags: tags,
  };
  console.log(phone);
  res.render("create-update-phones", { shopId: shopId, title: "Create new phone", method: "create", phone });
});

app.get("/api/tvs/create/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  console.log(shopId);
  const tags = "sony tv led smart black";
  var tv = {
    tvId: "",
    manufacturer: "SONY",
    type: "Televizor KDL50WF665 SMART (Crni)",
    productType: "tv",
    subtype: "smarttv",
    screenType: "LED",
    screenDiagonal: '50" (127 cm)',
    resolution: "1080p Full HD",
    digitalTuner: "DVB-T/T2/C/S/S2",
    operativeSystem: "Linux",
    price: "59.999RSD",
    tags: tags,
  };
  console.log(tv);
  res.render("create-update-tvs", { shopId: shopId, title: "Create new tv", method: "create", tv });
});

app.get("/api/cart/:userId", (req, res) => {
  const userId = req.params.userId;

  res.render("cart", { userId: userId, title: "Cart for user with id: " + userId, method: "create" });
});

var getItems = async function (client, userId) {
  client.connect();
  const database = client.db("EShops");
  const carts = database.collection("carts");
  const filter = { userId: userId };
  const searchCursor = await carts.find(filter);
  const res = await searchCursor.toArray();
  return res;
};

app.get("/cart/update/computer/:computerId", (req, res) => {
  const computerId = req.params.computerId;
  getUser(client).then((user) => {
    console.log("USER", user);
    addToCartComputer(client, computerId).then((id) => {
      getItems(client, user[0].userId).then((items) => {
        console.log("ITEMS", items);
        res.render("cart", { title: "", method: "", items: items, userId: user[0].userId });
      });
    });
  });
});

app.get("/cart/update/tv/:tvId", (req, res) => {
  const tvId = req.params.tvId;
  getUser(client).then((user) => {
    console.log("USER", user);
    addToCartTv(client, tvId).then((id) => {
      getItems(client, user[0].userId).then((items) => {
        console.log("ITEMS", items);
        res.render("cart", { title: "", method: "", items: items, userId: user[0].userId });
      });
    });
  });
});

app.get("/cart/update/phone/:phoneId", (req, res) => {
  const phoneId = req.params.phoneId;
  getUser(client).then((user) => {
    console.log("USER", user);
    addToCartPhone(client, phoneId).then((id) => {
      getItems(client, user[0].userId).then((items) => {
        console.log("ITEMS", items);
        res.render("cart", { title: "", method: "", items: items, userId: user[0].userId });
      });
    });
  });
});

app.get("/cart/:userId", (req, res) => {
  const userId = req.params.userId;
  console.log(userId);
  getItems(client, userId).then((items) => {
    console.log("ITEMS", items);
    res.render("cart", { title: "", method: "", items: items, userId: userId });
  });
});

app.get("/api/computers/create/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  console.log(shopId);
  const tags = "apple iphone black orange white";
  var computer = {
    computerId: "",
    manufacturer: "Apple",
    type: "MacBook Air 13 Retina",
    productType: "computer",
    subtype: "Laptop",
    processorModel: "Intel® Core™ i3 1000NG4 do 3.2GHz",
    screenDiagonal: "13.3",
    graphicCardType: "Integrisana Iris Plus Graphics G4",
    ramMemory: 8,
    hdd_ssd: "256GB SSD",
    price: "86.390 RSD",
    tags: tags,
  };
  console.log(computer);
  res.render("create-update-computers", { shopId: shopId, title: "Create new computer", method: "create", computer });
});

app.get("/api/shops/add-product/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  console.log(shopId);
  res.render("create-products", { shopId: shopId });
});

app.get("/api/users/delete/:id", (req, res) => {
  const userId = req.params.id;
  console.log(userId);
  deleteUser(client, userId);
  res.render("login", { alertType: "success", msg: "User deleted, please login", logged: false, notlogged: true });
});

app.get("/api/phones/delete/:id", (req, res) => {
  const phoneId = req.params.id;
  console.log(phoneId);
  deletePhone(client, phoneId);
  res.render("index", { alertType: "success", msg: "Phone deleted", logged: true, notlogged: false });
});

app.get("/api/computers/delete/:id", (req, res) => {
  const computerId = req.params.id;
  console.log(computerId);
  deleteComputer(client, computerId);
  res.render("index", { alertType: "success", msg: "Computer deleted", logged: true, notlogged: false });
});

app.get("/api/tvs/delete/:id", (req, res) => {
  const tvId = req.params.id;
  console.log(tvId);
  deleteTv(client, tvId);
  res.render("index", { alertType: "success", msg: "Tv deleted", logged: true, notlogged: false });
});

app.get("/api/shops/delete/:id", (req, res) => {
  const shopId = req.params.id;
  console.log(shopId);
  deleteShop(client, shopId);
  res.render("index", { alertType: "success", msg: "Shop deleted", logged: true, notlogged: false });
});

app.get("/api/phones/show/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  getPhonesByShopId(client, shopId).then((phones) => {
    console.log(phones);
    res.render("phones", { phones });
  });
});

app.get("/api/computers/show/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  getComputersByShopId(client, shopId).then((computers) => {
    console.log(computers);
    res.render("computers", { computers });
  });
});

app.get("/api/tvs/show/:shopId", (req, res) => {
  const shopId = req.params.shopId;
  getTvsByShopId(client, shopId).then((tvs) => {
    console.log(tvs);
    res.render("tvs", { tvs });
  });
});

app.get("/api/users/update/:id", async (req, res) => {
  var id = req.params.id;
  console.log(id);
  getUserById(client, id).then((user) => {
    console.log(user);
    res.render("register", {
      id,
      method: "update",
      title: "Update user",
      user: user[0],
    });
  });
});

app.get("/api/tvs/update/:tvId", async (req, res) => {
  var tvId = req.params.tvId;
  getTvById(client, tvId).then((tv) => {
    console.log(tv);
    var newTags = "";
    tv[0].tags.forEach((tag) => {
      newTags = newTags + tag + " ";
    });
    tv[0].tags = newTags;
    res.render("create-update-tvs", {
      shopId: tv[0].shopId,
      tvId,
      method: "update",
      title: "Update tv",
      tv: tv[0],
    });
  });
});

app.get("/api/computers/update/:computerId", async (req, res) => {
  var computerId = req.params.computerId;
  getComputerById(client, computerId).then((computer) => {
    console.log(computer);
    var newTags = "";
    computer[0].tags.forEach((tag) => {
      newTags = newTags + tag + " ";
    });
    computer[0].tags = newTags;
    res.render("create-update-computers", {
      shopId: computer[0].shopId,
      computerId,
      method: "update",
      title: "Update computer",
      computer: computer[0],
    });
  });
});

app.get("/api/phones/update/:phoneId", async (req, res) => {
  var phoneId = req.params.phoneId;
  getPhoneById(client, phoneId).then((phone) => {
    console.log(phone);
    var newTags = "";
    phone[0].tags.forEach((tag) => {
      newTags = newTags + tag + " ";
    });
    phone[0].tags = newTags;
    res.render("create-update-phones", {
      shopId: phone[0].shopId,
      phoneId,
      method: "update",
      title: "Update phone",
      phone: phone[0],
    });
  });
});

app.get("/api/shops/update/:shopId", async (req, res) => {
  var shopId = req.params.shopId;
  getShopById(client, shopId).then((shop) => {
    console.log(shop);
    res.render("create-update-shops", {
      shopId: shop[0].shopId,
      method: "update",
      title: "Update shop",
      shop: shop[0],
    });
  });
});

app.get("/api/deleteDatabase", async (req, res) => {
  deleteDatabase(client);

  var user = {
    username: "BK2O198",
    password: "12345678",
    role: "admin",
    name: { title: "mr", firstName: "Dusan", lastName: "Mitrovic" },
    gender: "male",
    email: "dusanmitrovic@elfak.rs",
    location: {
      street: "9278 new road",
      city: "Kilcoole",
      state: "waterford",
      postcode: "93027",
      coordinates: { latitude: "20.9267", longitude: "-7.9310" },
      timezone: { offset: "-3:30", description: "Newfoundland" },
    },
    dateOfBirth: { date: "1993-07-20T09:44:18.674Z", age: 27 },
    dateOfRegistration: { date: "2015-2-1", age: 0 },
    nationality: "IE",
    picture: {
      large: "https://randomuser.me/api/portraits/men/75.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/75.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
    },
    phone: {
      type: "home",
      number: "0611234567",
    },
  };
  res.render("register", {
    id: "",
    method: "create",
    title: "Create user",
    user,
  });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
