const crypto = require("crypto");

function hashPassword(username, password) {
  const s = username + ":" + password;
  return crypto.createHash("sha256").update(s).digest("hex");
}

module.exports = {
  hashPassword,
};
