const express = require("express");
const routerCreateUpdateShops = express.Router();
const randomstring = require("randomstring");
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const { v4: uuidv4 } = require("uuid");
const client = require("./mongo-db").client;
var _ = require("lodash");

var create = async function (client, newShop) {
  try {
    client.connect();
    const database = client.db("EShops");
    const shops = database.collection("shops");
    const doc = newShop;

    const shopId = { shopId: newShop.shopId };
    const find = await shops.findOne(shopId);
    console.log(find);
    if (find == null) {
      const result = await shops.insertOne(doc);
      console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
      console.log("Shop created!");
    } else {
      console.log("Shop already exists!");
    }
  } finally {
  }
};

async function update(client, modifiedShop, id) {
  try {
    const database = client.db("EShops");
    const shops = database.collection("shops");

    const filter = { shopId: id };

    const options = { upsert: true };
    const updateDoc = {
      $set: {
        shopId: modifiedShop.shopId,
        name: modifiedShop.name,
        location: {
          street: modifiedShop.location.street,
          city: modifiedShop.location.city,
          state: modifiedShop.location.state,
          postcode: modifiedShop.location.postcode,
          coordinates: {
            latitude: modifiedShop.location.coordinates.latitude,
            longitude: modifiedShop.location.coordinates.longitude,
          },
          timezone: {
            offset: modifiedShop.location.timezone.offset,
            description: modifiedShop.location.timezone.description,
          },
        },
      },
    };
    const result = await shops.updateOne(filter, updateDoc, options);
    if (result.modifiedCount === 0 && result.upsertedCount === 0) {
      console.log("No changes made to the collection.");
    } else {
      if (result.matchedCount === 1) {
        console.log("Matched " + result.matchedCount + " documents.");
      }
      if (result.modifiedCount === 1) {
        console.log("Updated one document.");
      }
      if (result.upsertedCount === 1) {
        console.log("Inserted one new document with an _id of " + result.upsertedId._id);
      }
    }
  } finally {
  }
}

routerCreateUpdateShops.get("/", (req, res) => {
  var shop = {
    shopId: "",
    name: "Shop 1",
    location: {
      street: "9278 new road",
      city: "Kilcoole",
      state: "waterford",
      postcode: "93027",
      coordinates: { latitude: "20.9267", longitude: "-7.9310" },
      timezone: { offset: "-3:30", description: "Newfoundland" },
    },
  };
  res.render("create-update-shops", {
    title: "Create new shop",
    shop,
    method: "create",
  });
});

routerCreateUpdateShops.get("/:id", (req, res) => {});

routerCreateUpdateShops.post("/", (req, res) => {
  console.log("BODY", req.body);

  if (req.body.method === "create") {
    const id = uuidv4();
    var newShop = {
      shopId: id,
      name: req.body.name,
      location: {
        street: req.body.street,
        city: req.body.city,
        state: req.body.state,
        postcode: req.body.postcode,
        coordinates: {
          latitude: req.body.latitude,
          longitude: req.body.longitude,
        },
        timezone: {
          offset: req.body.offset,
          description: req.body.description,
        },
      },
    };
    console.log("New shop", newShop);

    var result = create(client, newShop);

    res.render("index", {
      alertType: "success",
      msg: "Shop successfully created!",
      status: null,
      logged: true,
      notlogged: false,
    });
  } else if (req.body.method === "update") {
    var modifiedShop = {
      shopId: req.body.shopId,
      name: req.body.name,
      location: {
        street: req.body.street,
        city: req.body.city,
        state: req.body.state,
        postcode: req.body.postcode,
        coordinates: {
          latitude: req.body.latitude,
          longitude: req.body.longitude,
        },
        timezone: {
          offset: req.body.offset,
          description: req.body.description,
        },
      },
    };

    console.log("Modified shop", modifiedShop);

    var result = update(client, modifiedShop, modifiedShop.shopId);
  }

  res.render("index", {
    alertType: "success",
    msg: "Shop successfully updated!",
    status: null,
    logged: true,
    notlogged: false,
  });
});

routerCreateUpdateShops.put("/put/:id", (req, res) => {});

routerCreateUpdateShops.get("delete/:id", (req, res) => {});

module.exports = routerCreateUpdateShops;
