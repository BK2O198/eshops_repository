const express = require("express");
const { result } = require("lodash");
const routerUsers = express.Router();
const client = require("./mongo-db").client;

var update = async function (client, modifiedUser) {
  await client.connect();
  const database = client.db("EShops");
  const movies = database.collection("users");
  // create a filter for a movie to update
  const filter = { id: modifiedUser.id };
  console.log(filter);
  // this option instructs the method to create a document if no documents match the filter
  const options = { upsert: true };
  // create a document that sets the plot of the movie
  const updateDoc = {
    $set: {
      id: modifiedUser.id,
      api_key: modifiedUser.api_key,
      username: modifiedUser.username,
      password: modifiedUser.password,
      role: modifiedUser.role,
      name: {
        title: modifiedUser.name.title,
        firstName: modifiedUser.name.firstName,
        lastName: modifiedUser.name.lastName,
      },
      gender: modifiedUser.gender,
      email: modifiedUser.email,
      location: {
        street: modifiedUser.location.street,
        city: modifiedUser.location.city,
        state: modifiedUser.location.state,
        postcode: modifiedUser.location.postcode,
        coordinates: { latitude: modifiedUser.coordinates.latitude, longitude: modifiedUser.coordinates.longitude },
        timezone: {
          offset: modifiedUser.coordinates.timezone,
          description: modifiedUser.coordinates.timezone.description,
        },
      },
      dateOfBirth: { date: modifiedUser.dateOfBirth.date, age: modifiedUser.dateOfBirth.age },
      dateOfRegistration: { date: modifiedUser.dateOfRegistration.date, age: modifiedUser.dateOfRegistration.age },
      nationality: modifiedUser.nationality,
      picture: {
        large: modifiedUser.picture.large,
        medium: modifiedUser.picture.medium,
        thumbnail: modifiedUser.picture.thumbnail,
      },
      phone: {
        type: modifiedUser.phone.type,
        number: modifiedUser.phone.number,
      },
    },
  };
  const result = await users.updateOne(filter, updateDoc, options);
  console.log(`${result.matchedCount} document(s) matched the filter, updated ${result.modifiedCount} document(s)`);
};

var getUsers = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const searchCursor = await users.find();
  const result = await searchCursor.toArray();

  return result;
};

routerUsers.get("/", async (req, res, next) => {
  getUsers(client).then((users) => {
    console.log(users);
    res.render("users", { users });
  });
});

routerUsers.get("/:id", async (req, res, next) => {});

routerUsers.post("/", async (req, res, next) => {});

routerUsers.put("/:id", async (req, res, next) => {});

routerUsers.delete("/:id", async (req, res, next) => {});

module.exports = routerUsers;
