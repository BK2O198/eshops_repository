const express = require("express");
const routerRegister = express.Router();
const hashPassword = require("../../public/js/hashFunctions").hashPassword;
const UserCredentials = require("../../models/user-credentials");
const randomstring = require("randomstring");
const User = require("../../models/user").User;
const Name = require("../../models/user").Name;
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const DateOfBirth = require("../../models/user").DateOfBirth;
const DateOfRegistration = require("../../models/user").DateOfRegistration;
const Picture = require("../../models/user").Picture;
const PhoneNumber = require("../../models/user").PhoneNumber;
const { v4: uuidv4 } = require("uuid");
const client = require("./mongo-db").client;
var _ = require("lodash");

var create = async function (client, newUser) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const logovanje_korisnika = database.collection("logovanje_korisnika");
  const doc = newUser;
  const doc2 = {
    username: newUser.username,
    password: newUser.password,
    api_key: newUser.api_key,
  };

  const username = { username: newUser.username };
  const find = await users.findOne(username);
  console.log(find);
  if (find == null) {
    const result = await users.insertOne(doc);
    const result2 = await logovanje_korisnika.insertOne(doc2);

    console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
    console.log(`${result2.insertedCount} documents were inserted with the _id: ${result2.insertedId}`);
    console.log("User created!");
    return "User created";
  } else {
    return "User already exists!";
  }
};

async function update(client, modifiedUser, id) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const logovanje_korisnika = database.collection("logovanje_korisnika");

  const filter = { userId: id };
  const filter2 = { api_key: modifiedUser.api_key };
  const options = { upsert: true };

  const updateDoc = {
    $set: {
      userId: modifiedUser.userId,
      api_key: modifiedUser.api_key,
      username: modifiedUser.username,
      password: modifiedUser.password,
      role: modifiedUser.role,
      gender: modifiedUser.gender,
      email: modifiedUser.email,
      nationality: modifiedUser.nationality,
      name: {
        title: modifiedUser.name.title,
        firstName: modifiedUser.name.firstName,
        lastName: modifiedUser.name.lastName,
      },
      location: {
        street: modifiedUser.location.street,
        city: modifiedUser.location.city,
        state: modifiedUser.location.state,
        postcode: modifiedUser.location.postcode,
        coordinates: {
          latitude: modifiedUser.location.coordinates.latitude,
          longitude: modifiedUser.location.coordinates.longitude,
        },
        timezone: {
          offset: modifiedUser.location.timezone.offset,
          description: modifiedUser.location.timezone.description,
        },
      },
      dateOfBirth: { date: modifiedUser.dateOfBirth.date, age: modifiedUser.dateOfBirth.age },
      dateOfRegistration: { date: modifiedUser.dateOfRegistration.date, age: modifiedUser.dateOfRegistration.age },
      picture: {
        large: modifiedUser.picture.large,
        medium: modifiedUser.picture.medium,
        thumbnail: modifiedUser.picture.thumbnail,
      },
      phone: { type: modifiedUser.phone.type, number: modifiedUser.phone.number },
    },
  };
  const updateDoc2 = {
    $set: {
      username: modifiedUser.username,
      password: modifiedUser.password,
      api_key: modifiedUser.api_key,
    },
  };
  const result = await users.updateOne(filter, updateDoc, options);
  const result2 = await logovanje_korisnika.updateOne(filter2, updateDoc2, options);
  if (result.modifiedCount === 0 && result.upsertedCount === 0) {
    console.log("No changes made to the collection.");
  } else {
    if (result.matchedCount === 1) {
      console.log("Matched " + result.matchedCount + " documents.");
    }
    if (result.modifiedCount === 1) {
      console.log("Updated one document.");
    }
    if (result.upsertedCount === 1) {
      console.log("Inserted one new document with an _id of " + result.upsertedId._id);
    }
  }
  if (result2.modifiedCount === 0 && result2.upsertedCount === 0) {
    console.log("No changes made to the collection.");
  } else {
    if (result2.matchedCount === 1) {
      console.log("Matched " + result2.matchedCount + " documents.");
    }
    if (result2.modifiedCount === 1) {
      console.log("Updated one document.");
    }
    if (result2.upsertedCount === 1) {
      console.log("Inserted one new document with an _id of " + result2.upsertedId._id);
    }
  }
}

function getAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

routerRegister.get("/", (req, res) => {
  var date = Date.now();

  var user = {
    username: "BK2O198",
    password: "12345678",
    role: "admin",
    name: { title: "mr", firstName: "Dusan", lastName: "Mitrovic" },
    gender: "male",
    email: "dusanmitrovic@elfak.rs",
    location: {
      street: "9278 new road",
      city: "Kilcoole",
      state: "waterford",
      postcode: "93027",
      coordinates: { latitude: "20.9267", longitude: "-7.9310" },
      timezone: { offset: "-3:30", description: "Newfoundland" },
    },
    dateOfBirth: { date: "1993-07-20T09:44:18.674Z", age: 27 },
    dateOfRegistration: { date: date, age: 0 },
    nationality: "IE",
    picture: {
      large: "https://randomuser.me/api/portraits/men/75.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/75.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
    },
    phone: {
      type: "home",
      number: "0611234567",
    },
  };
  res.render("register", {
    title: "Create new user",
    user,
  });
});

routerRegister.get("/:id", (req, res) => {
  const query = "SELECT * FROM ecatalog.korisnici WHERE korisnikId = ?";
  const id = req.params.id;
  console.log("params", req.params.id);
  client.execute(query, [id], { prepare: true }).then(function (result) {
    res.render("register", {
      title: "Update user",
      user: {
        _id: result.rows[0].korisnikid.toString(),
        ime: result.rows[0].ime,
        prezime: result.rows[0].prezime,
        email: result.rows[0].email,
      },
    });
  });
});

routerRegister.post("/", (req, res) => {
  console.log("BODY", req.body);
  var currentTime = new Date();
  var month = currentTime.getMonth() + 1;
  var day = currentTime.getDate();
  var year = currentTime.getFullYear();
  var date_time = new Date(year, month, day);

  if (req.body.id == "") {
    const id = uuidv4();
    const api_key = uuidv4();
    var newUser = {};

    newUser.userId = id;
    newUser.api_key = api_key;
    newUser.username = req.body.username;
    newUser.password = req.body.password;
    newUser.role = req.body.role;
    newUser.gender = req.body.gender;
    newUser.email = req.body.email;
    newUser.nationality = req.body.nationality;

    //User - Name
    var name = {};
    name.title = req.body.title;
    name.firstName = req.body.firstName;
    name.lastName = req.body.lastName;
    newUser.name = name;

    newUser.nationality = req.body.nationality;

    //User - Location
    var location = {};
    location.street = req.body.street;
    location.city = req.body.city;
    location.state = req.body.state;
    location.postcode = req.body.postcode;
    newUser.location = location;

    //User - Location - Coordinates
    var coordinates = {};
    coordinates.latitude = req.body.latitude;
    coordinates.longitude = req.body.longitude;
    newUser.location.coordinates = coordinates;

    //User - Location - Timezone
    var timezone = {};
    timezone.offset = req.body.offset;
    timezone.description = req.body.description;
    newUser.location.timezone = timezone;

    //User - DateOfBirth
    var dateOfBirth = {};
    dateOfBirth.date = req.body.dateOfBirth_Date;
    dateOfBirth.age = getAge(req.body.dateOfBirth_Date);
    newUser.dateOfBirth = dateOfBirth;

    //User - DateOfRegistration
    var dateOfRegistration = {};
    dateOfRegistration.date = date_time;
    dateOfRegistration.age = 0;
    newUser.dateOfRegistration = dateOfRegistration;

    //User - Picture
    var picture = {};
    picture.large = req.body.large;
    picture.medium = req.body.medium;
    picture.thumbnail = req.body.thumbnail;
    newUser.picture = picture;

    var phone = {};
    phone.type = req.body.type;
    phone.number = req.body.phone;
    newUser.phone = phone;

    console.log("New user", newUser);

    var result = create(client, newUser);

    res.render("login", {
      alertType: "success",
      msg: result,
      status: null,
      logged: true,
      notlogged: false,
    });
  } else if (req.body.method === "update") {
    var modifiedUser = {};

    modifiedUser.userId = req.body.id;
    modifiedUser.api_key = req.body.api_key;
    modifiedUser.username = req.body.username;
    modifiedUser.password = req.body.password;
    modifiedUser.role = req.body.role;
    modifiedUser.gender = req.body.gender;
    modifiedUser.email = req.body.email;
    modifiedUser.nationality = req.body.nationality;

    //User - Name
    var name = {};
    name.title = req.body.title;
    name.firstName = req.body.firstName;
    name.lastName = req.body.lastName;
    modifiedUser.name = name;

    modifiedUser.nationality = req.body.nationality;

    //User - Location
    var location = {};
    location.street = req.body.street;
    location.city = req.body.city;
    location.state = req.body.state;
    location.postcode = req.body.postcode;
    modifiedUser.location = location;

    //User - Location - Coordinates
    var coordinates = {};
    coordinates.latitude = req.body.latitude;
    coordinates.longitude = req.body.longitude;
    modifiedUser.location.coordinates = coordinates;

    //User - Location - Timezone
    var timezone = {};
    timezone.offset = req.body.offset;
    timezone.description = req.body.description;
    modifiedUser.location.timezone = timezone;

    //User - DateOfBirth
    var dateOfBirth = {};
    dateOfBirth.date = req.body.dateOfBirth_Date;
    dateOfBirth.age = getAge(req.body.dateOfBirth_Date);
    modifiedUser.dateOfBirth = dateOfBirth;

    //User - DateOfRegistration
    var dateOfRegistration = {};
    dateOfRegistration.date = date_time;
    dateOfRegistration.age = 0;
    modifiedUser.dateOfRegistration = dateOfRegistration;

    //User - Picture
    var picture = {};
    picture.large = req.body.large;
    picture.medium = req.body.medium;
    picture.thumbnail = req.body.thumbnail;
    modifiedUser.picture = picture;

    var phone = {};
    phone.type = req.body.type;
    phone.number = req.body.phone;
    modifiedUser.phone = phone;

    console.log("Modified user", modifiedUser);

    var result = update(client, modifiedUser, modifiedUser.userId);
  }

  res.redirect("/");
});

routerRegister.put("/put/:id", (req, res) => {});

routerRegister.get("delete/:id", (req, res) => {});

module.exports = routerRegister;
