const express = require("express");
const routerLogin = express.Router();
const hashPassword = require("../../public/js/hashFunctions").hashPassword;
const client = require("./mongo-db").client;

var _ = require("lodash");

var getUser = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik = await ulogovani_korisnik.find();
  const user = await korisnik.toArray();
  return user;
};

var login = async function (session, username, password) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const logovanje_korisnika = database.collection("logovanje_korisnika");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  ulogovani_korisnik.drop(function (err, result) {
    if (result) console.log("Collection successfully deleted.");
    return;
  });
  const filter = { username: username };
  const find = await logovanje_korisnika.findOne(filter);
  var dbuser = await users.findOne();
  if (dbuser === null || dbuser.username === null || dbuser.password === null) {
    return {
      alertType: "danger",
      msg: "There are no users in database",
      status: null,
    };
  }
  if (find === null) {
    //console.log("pass 1");
    return {
      alertType: "danger",
      msg: "Username does not exist",
      status: null,
    };
  } else {
    //console.log("pass 2");
    //console.log(find);
    var api_key = { api_key: find.api_key };
    var dbUser = await users.findOne(api_key);
    if (dbUser === null || dbUser.username === null || dbUser.password === null) {
      return {
        alertType: "danger",
        msg: "There are no users in database",
        status: null,
      };
    }
    if (dbUser.password != password) {
      return {
        alertType: "danger",
        msg: "Wrong password",
        status: null,
      };
    }
    console.log("pass 3");
    const ulogovani_korisnik = database.collection("ulogovani_korisnik");
    const doc = { userId: dbUser.userId };
    const result = await ulogovani_korisnik.insertOne(doc);

    console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
    return {
      token: dbUser.api_key,
    };
  }
};

routerLogin.get("/", async (req, res, next) => {
  res.render("login", { title: "Login" });
});

routerLogin.get("/:id", async (req, res, next) => {});

routerLogin.post("/", async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  login(client, username, password).then((result) => {
    console.log("RESULT", result);
    if (result.msg === "Username does not exist") {
      res.render("login", {
        title: "Login",
        alertType: "danger",
        msg: "Username does not exist",
        status: null,
      });
    } else if (result.msg === "Wrong password") {
      res.render("login", { title: "Login", alertType: "danger", msg: "Wrong password", status: null });
    } else if (result.msg === "There are no users in database") {
      var user = {
        username: "BK2O198",
        password: "12345678",
        role: "admin",
        name: { title: "mr", firstName: "Dusan", lastName: "Mitrovic" },
        gender: "male",
        email: "dusanmitrovic@elfak.rs",
        location: {
          street: "9278 new road",
          city: "Kilcoole",
          state: "waterford",
          postcode: "93027",
          coordinates: { latitude: "20.9267", longitude: "-7.9310" },
          timezone: { offset: "-3:30", description: "Newfoundland" },
        },
        dateOfBirth: { date: "1993-07-20T09:44:18.674Z", age: 27 },
        dateOfRegistration: { date: "2021-05-10", age: 0 },
        nationality: "IE",
        picture: {
          large: "https://randomuser.me/api/portraits/men/75.jpg",
          medium: "https://randomuser.me/api/portraits/med/men/75.jpg",
          thumbnail: "https://randomuser.me/api/portraits/thumb/men/75.jpg",
        },
        phone: {
          type: "home",
          number: "0611234567",
        },
      };
      res.render("register", {
        title: "Register",
        alertType: "danger",
        msg: "There are no users in database",
        status: null,
        user: user,
      });
    } else {
      getUser(client).then((user) => {
        if (user[0] == undefined) {
          res.render("login", {
            alertType: "danger",
            msg: "Please try again",
            status: null,
            logged: true,
            notlogged: false,
          });
        } else {
          res.render("index", {
            alertType: "info",
            msg: "User: " + user[0].userId,
            status: null,
            logged: true,
            notlogged: false,
          });
        }
      });
    }
  });
});

routerLogin.put("/:id", async (req, res, next) => {});

routerLogin.delete("/:id", async (req, res, next) => {});

module.exports = routerLogin;
