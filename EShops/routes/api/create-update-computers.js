const express = require("express");
const routerCreateUpdateComputers = express.Router();
const randomstring = require("randomstring");
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const { v4: uuidv4 } = require("uuid");
const client = require("./mongo-db").client;
var _ = require("lodash");

var create = async function (client, newComputer) {
  try {
    client.connect();
    const database = client.db("EShops");
    const computers = database.collection("computers");
    const doc = newComputer;

    const type = { type: newComputer.type };
    const find = await computers.findOne(type);
    console.log(find);
    if (find == null) {
      const result = await computers.insertOne(doc);
      console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
      console.log("Computer created!");
    } else {
      console.log("Computer already exists!");
    }
  } finally {
  }
};

async function update(client, modifiedComputer, id) {
  try {
    client.connect();
    const database = client.db("EShops");
    const computers = database.collection("computers");

    const filter = { computerId: id };

    const options = { upsert: true };

    const updateDoc = {
      $set: {
        computerId: modifiedComputer.computerId,
        shopId: modifiedComputer.shopId,
        manufacturer: modifiedComputer.manufacturer,
        type: modifiedComputer.type,
        productType: modifiedComputer.productType,
        subtype: modifiedComputer.subtype,
        processorModel: modifiedComputer.processorModel,
        screenDiagonal: modifiedComputer.screenDiagonal,
        graphicCardType: modifiedComputer.graphicCardType,
        ramMemory: modifiedComputer.ramMemory,
        hdd_ssd: modifiedComputer.hdd_ssd,
        price: modifiedComputer.price,
        tags: modifiedComputer.tags,
      },
    };
    const result = await computers.updateOne(filter, updateDoc, options);
    if (result.modifiedCount === 0 && result.upsertedCount === 0) {
      console.log("No changes made to the collection.");
    } else {
      if (result.matchedCount === 1) {
        console.log("Matched " + result.matchedCount + " documents.");
      }
      if (result.modifiedCount === 1) {
        console.log("Updated one document.");
      }
      if (result.upsertedCount === 1) {
        console.log("Inserted one new document with an _id of " + result.upsertedId._id);
      }
    }
  } finally {
  }
}

routerCreateUpdateComputers.get("/", (req, res) => {
  const tags = "apple iphone black orangeddd white";
  var computer = {
    computerId: "",
    manufacturer: "Apple",
    type: "MacBook Air 13 Retina",
    productType: "computer",
    subtype: "Laptop",
    processorModel: "Intel® Core™ i3 1000NG4 do 3.2GHz",
    screenDiagonal: "13.3",
    graphicCardType: "Integrisana Iris Plus Graphics G4",
    ramMemory: 8,
    hdd_ssd: "256GB SSD",
    price: "86.390 RSD",
    tags: tags,
  };
  res.render("create-update-computers", {
    title: "Create new c  omputer",
    computer,
    method: "create",
  });
});

routerCreateUpdateComputers.get("/:id", (req, res) => {});

routerCreateUpdateComputers.post("/", (req, res) => {
  console.log("BODY", req.body);

  if (req.body.method === "create") {
    const id = uuidv4();
    var newComputer = {
      computerId: id,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      productType: req.body.productType,
      subtype: req.body.subtype,
      processorModel: req.body.processorModel,
      screenDiagonal: req.body.screenDiagonal,
      graphicCardType: req.body.graphicCardType,
      ramMemory: req.body.ramMemory,
      hdd_ssd: req.body.hdd_ssd,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };
    console.log("New computer", newComputer);

    var result = create(client, newComputer);

    res.render("index", {
      alertType: "success",
      msg: "Computer successfully created!",
      status: null,
      logged: true,
      notlogged: false,
    });
  } else if (req.body.method === "update") {
    var modifiedComputer = {
      computerId: req.body.computerId,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      productType: req.body.productType,
      subtype: req.body.subtype,
      processorModel: req.body.processorModel,
      screenDiagonal: req.body.screenDiagonal,
      graphicCardType: req.body.graphicCardType,
      ramMemory: req.body.ramMemory,
      hdd_ssd: req.body.hdd_ssd,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };

    console.log("Modified computer", modifiedComputer);

    var result = update(client, modifiedComputer);
  }

  res.render("index", {
    alertType: "success",
    msg: "Computer successfully updated!",
    status: null,
    logged: true,
    notlogged: false,
  });
});

routerCreateUpdateComputers.put("/put/:id", (req, res) => {});

routerCreateUpdateComputers.get("delete/:id", (req, res) => {});

module.exports = routerCreateUpdateComputers;
