const express = require("express");
const { result } = require("lodash");
const routerCart = express.Router();
const client = require("./mongo-db").client;

routerCart.get("/", async (req, res, next) => {});

routerCart.get("/:userId", async (req, res, next) => {});

routerCart.post("/", async (req, res, next) => {});

routerCart.put("/:id", async (req, res, next) => {});

routerCart.delete("/:id", async (req, res, next) => {});

module.exports = routerCart;
