const express = require("express");
const routerCreateUpdateTvs = express.Router();
const randomstring = require("randomstring");
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const { v4: uuidv4 } = require("uuid");
const client = require("./mongo-db").client;
var _ = require("lodash");

var create = async function (client, newTv) {
  try {
    client.connect();
    const database = client.db("EShops");
    const tvs = database.collection("tvs");
    const doc = newTv;

    const type = { type: newTv.type };
    const find = await tvs.findOne(type);
    console.log(find);
    if (find == null) {
      const result = await tvs.insertOne(doc);
      console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
      console.log("Phone created!");
    } else {
      console.log("Phone already exists!");
    }
  } finally {
  }
};

async function update(client, modifiedTv, id) {
  try {
    client.connect();
    const database = client.db("EShops");
    const tvs = database.collection("tvs");

    const filter = { tvId: id };

    const options = { upsert: true };

    const updateDoc = {
      $set: {
        tvId: modifiedTv.tvId,
        shopId: modifiedTv.shopId,
        manufacturer: modifiedTv.manufacturer,
        type: modifiedTv.type,
        productType: modifiedTv.productType,
        subtype: modifiedTv.subtype,
        screenType: modifiedTv.screenType,
        screenDiagonal: modifiedTv.screenDiagonal,
        resolution: modifiedTv.resolution,
        digitalTuner: modifiedTv.digitalTuner,
        operativeSystem: modifiedTv.operativeSystem,
        price: modifiedTv.price,
        tags: modifiedTv.tags,
      },
    };
    const result = await tvs.updateOne(filter, updateDoc, options);
    if (result.modifiedCount === 0 && result.upsertedCount === 0) {
      console.log("No changes made to the collection.");
    } else {
      if (result.matchedCount === 1) {
        console.log("Matched " + result.matchedCount + " documents.");
      }
      if (result.modifiedCount === 1) {
        console.log("Updated one document.");
      }
      if (result.upsertedCount === 1) {
        console.log("Inserted one new document with an _id of " + result.upsertedId._id);
      }
    }
  } finally {
  }
}

routerCreateUpdateTvs.get("/", (req, res) => {
  const tags = "sony tv led smart black";
  var tv = {
    tvId: "",
    manufacturer: "SONY",
    type: "Televizor KDL50WF665 SMART (Crni)",
    productType: "tv",
    subtype: "smarttv",
    screenType: "LED",
    screenDiagonal: '50" (127 cm)',
    resolution: "1080p Full HD",
    digitalTuner: "DVB-T/T2/C/S/S2",
    operativeSystem: "Linux",
    price: "59.999RSD",
    tags: tags,
  };
  res.render("create-update-tvs", {
    title: "Create new tv",
    tv,
    method: "create",
  });
});

routerCreateUpdateTvs.get("/:id", (req, res) => {});

routerCreateUpdateTvs.post("/", (req, res) => {
  console.log("BODY", req.body);

  if (req.body.method === "create") {
    const id = uuidv4();
    var newTv = {
      tvId: id,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      productType: req.body.productType,
      subtype: req.body.subtype,
      screenType: req.body.screenType,
      screenDiagonal: req.body.screenDiagonal,
      resolution: req.body.resolution,
      digitalTuner: req.body.digitalTuner,
      operativeSystem: req.body.operativeSystem,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };
    console.log("New tv", newTv);

    var result = create(client, newTv);

    res.render("index", {
      alertType: "success",
      msg: "Tv successfully created!",
      status: null,
      logged: true,
      notlogged: false,
    });
  } else if (req.body.method === "update") {
    var modifiedTv = {
      tvId: req.body.tvId,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      productType: req.body.productType,
      subtype: req.body.subtype,
      screenType: req.body.screenType,
      screenDiagonal: req.body.screenDiagonal,
      resolution: req.body.resolution,
      digitalTuner: req.body.digitalTuner,
      operativeSystem: req.body.operativeSystem,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };

    console.log("Modified tv", modifiedTv);

    var result = update(client, modifiedTv, modifiedTv.tvId);
  }

  res.render("index", {
    alertType: "success",
    msg: "Tv successfully updated!",
    status: null,
    logged: true,
    notlogged: false,
  });
});

routerCreateUpdateTvs.put("/put/:id", (req, res) => {});

routerCreateUpdateTvs.get("delete/:id", (req, res) => {});

module.exports = routerCreateUpdateTvs;
