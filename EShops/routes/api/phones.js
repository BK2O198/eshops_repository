const express = require("express");
const { result } = require("lodash");
const routerPhones = express.Router();
const client = require("./mongo-db").client;

var getUserById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const filter = { userId: id };
  const searchCursor = await users.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getUser = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik = await ulogovani_korisnik.find();
  const user = await korisnik.toArray();
  return user;
};

var getPhones = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const phones = database.collection("phones");
  const searchCursor = await phones.find();
  const result = await searchCursor.toArray();

  return result;
};

routerPhones.get("/", async (req, res, next) => {
  getUser(client).then((user) => {
    console.log(user[0].userId);
    const userId = user[0].userId;
    getUserById(client, userId).then((u) => {
      console.log(u);
      const role = u[0].role;
      console.log(role);
      var admin = false;
      var seller = false;
      var buyer = false;
      if (role === "admin") {
        admin = true;
        seller = false;
        buyer = false;
      }
      if (role === "seller") {
        admin = false;
        seller = true;
        buyer = false;
      }
      if (role === "buyer") {
        admin = false;
        seller = false;
        buyer = true;
      }
      console.log("pass phones 1");
      getPhones(client).then((phones) => {
        console.log(phones);
        res.render("phones", { phones, admin: admin, seller: seller, buyer: buyer, role: role });
      });
    });
  });
});

routerPhones.get("/:id", async (req, res, next) => {});

routerPhones.post("/", async (req, res, next) => {});

routerPhones.put("/:id", async (req, res, next) => {});

routerPhones.delete("/:id", async (req, res, next) => {});

module.exports = routerPhones;
