const express = require("express");
const routerCreateUpdatePhones = express.Router();
const randomstring = require("randomstring");
const Location = require("../../models/user").Location;
const Coordinates = require("../../models/user").Coordinates;
const Timezone = require("../../models/user").Timezone;
const { v4: uuidv4 } = require("uuid");
const client = require("./mongo-db").client;
var _ = require("lodash");

var create = async function (client, newPhone) {
  try {
    client.connect();
    const database = client.db("EShops");
    const phones = database.collection("phones");
    const doc = newPhone;

    const type = { type: newPhone.type };
    const find = await phones.findOne(type);
    console.log(find);
    if (find == null) {
      const result = await phones.insertOne(doc);
      console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`);
      console.log("Phone created!");
    } else {
      console.log("Phone already exists!");
    }
  } finally {
  }
};

async function update(client, modifiedPhone, id) {
  try {
    const database = client.db("EShops");
    const phones = database.collection("phones");
    const filter = { phoneId: id };

    const options = { upsert: true };

    const updateDoc = {
      $set: {
        phoneId: modifiedPhone.phoneId,
        shopId: modifiedPhone.shopId,
        manufacturer: modifiedPhone.manufacturer,
        type: modifiedPhone.type,
        subtype: modifiedPhone.subtype,
        screenDiagonal: modifiedPhone.screenDiagonal,
        ramMemory: modifiedPhone.ramMemory,
        internalMemory: modifiedPhone.internalMemory,
        camera: modifiedPhone.camera,
        bateryCapacity: modifiedPhone.bateryCapacity,
        resolution: modifiedPhone.resolution,
        screenType: modifiedPhone.screenType,
        price: modifiedPhone.price,
        tags: modifiedPhone.tags,
      },
    };
    const result = await phones.updateOne(filter, updateDoc, options);
    if (result.modifiedCount === 0 && result.upsertedCount === 0) {
      console.log("No changes made to the collection.");
    } else {
      if (result.matchedCount === 1) {
        console.log("Matched " + result.matchedCount + " documents.");
      }
      if (result.modifiedCount === 1) {
        console.log("Updated one document.");
      }
      if (result.upsertedCount === 1) {
        console.log("Inserted one new document with an _id of " + result.upsertedId._id);
      }
    }
  } finally {
  }
}

routerCreateUpdatePhones.get("/", (req, res) => {
  const tags = "apple iphone black orange white";
  var phone = {
    phoneId: "",
    manufacturer: "Apple",
    type: "iPhone XR",
    productType: "phone",
    subtype: "smartphone",
    screenDiagonal: "6.1",
    ramMemory: 3,
    internalMemory: 128,
    camera: "212.0 Mpix",
    bateryCapacity: "2940 mAh",
    resolution: "1792 x 828",
    screenType: "IPS",
    price: "86.390 RSD",
    tags: tags,
  };
  res.render("create-update-phones", {
    title: "Create new phone",
    phone,
    method: "create",
  });
});

routerCreateUpdatePhones.get("/:id", (req, res) => {});

routerCreateUpdatePhones.post("/", (req, res) => {
  console.log("BODY", req.body);

  if (req.body.method === "create") {
    const id = uuidv4();
    var newPhone = {
      phoneId: id,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      subtype: req.body.subtype,
      screenDiagonal: req.body.screenDiagonal,
      ramMemory: req.body.ramMemory,
      internalMemory: req.body.internalMemory,
      camera: req.body.camera,
      bateryCapacity: req.body.bateryCapacity,
      resolution: req.body.resolution,
      screenType: req.body.screenType,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };
    console.log("New phone", newPhone);

    var result = create(client, newPhone);

    res.render("index", {
      alertType: "success",
      msg: "Phone successfully created!",
      status: null,
      logged: true,
      notlogged: false,
    });
  } else if (req.body.method === "update") {
    var modifiedPhone = {
      phoneId: req.body.phoneId,
      shopId: req.body.shopId,
      manufacturer: req.body.manufacturer,
      type: req.body.type,
      subtype: req.body.subtype,
      screenDiagonal: req.body.screenDiagonal,
      ramMemory: req.body.ramMemory,
      internalMemory: req.body.internalMemory,
      camera: req.body.camera,
      bateryCapacity: req.body.bateryCapacity,
      resolution: req.body.resolution,
      screenType: req.body.screenType,
      price: req.body.price,
      tags: req.body.tags.split(" "),
    };

    console.log("Modified phone", modifiedPhone);

    var result = update(client, modifiedPhone, modifiedPhone.phoneId);
  }

  res.render("index", {
    alertType: "success",
    msg: "Phone successfully updated!",
    status: null,
    logged: true,
    notlogged: false,
  });
});

routerCreateUpdatePhones.put("/put/:id", (req, res) => {});

routerCreateUpdatePhones.get("delete/:id", (req, res) => {});

module.exports = routerCreateUpdatePhones;
