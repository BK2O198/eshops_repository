const express = require("express");
const { result } = require("lodash");
const routerComputers = express.Router();
const client = require("./mongo-db").client;

var getUserById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const filter = { userId: id };
  const searchCursor = await users.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getUser = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik = await ulogovani_korisnik.find();
  const user = await korisnik.toArray();
  return user;
};

var getComputers = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const computers = database.collection("computers");
  const searchCursor = await computers.find();
  const result = await searchCursor.toArray();

  return result;
};

routerComputers.get("/", async (req, res, next) => {
  getUser(client).then((user) => {
    console.log(user[0].userId);
    const userId = user[0].userId;
    getUserById(client, userId).then((u) => {
      console.log(u);
      const role = u[0].role;
      console.log(role);
      var admin = false;
      var seller = false;
      var buyer = false;
      if (role === "admin") {
        admin = true;
        seller = false;
        buyer = false;
      }
      if (role === "seller") {
        admin = false;
        seller = true;
        buyer = false;
      }
      if (role === "buyer") {
        admin = false;
        seller = false;
        buyer = true;
      }
      console.log("pass computers 1");
      getComputers(client).then((computers) => {
        console.log(computers);
        res.render("computers", { computers, admin: admin, seller: seller, buyer: buyer, role: role });
      });
    });
  });
});

routerComputers.get("/:id", async (req, res, next) => {});

routerComputers.post("/", async (req, res, next) => {});

routerComputers.put("/:id", async (req, res, next) => {});

routerComputers.delete("/:id", async (req, res, next) => {});

module.exports = routerComputers;
