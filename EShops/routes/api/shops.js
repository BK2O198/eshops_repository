const express = require("express");
const { result } = require("lodash");
const routerShops = express.Router();
const client = require("./mongo-db").client;

var getUserById = async function (client, id) {
  client.connect();
  const database = client.db("EShops");
  const users = database.collection("users");
  const filter = { userId: id };
  const searchCursor = await users.find(filter);
  const result = await searchCursor.toArray();
  return result;
};

var getUser = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const ulogovani_korisnik = database.collection("ulogovani_korisnik");
  const korisnik = await ulogovani_korisnik.find();
  const user = await korisnik.toArray();
  return user;
};

var getShops = async function (client) {
  client.connect();
  const database = client.db("EShops");
  const shops = database.collection("shops");
  const searchCursor = await shops.find();
  const result = await searchCursor.toArray();

  return result;
};

routerShops.get("/", async (req, res, next) => {
  getUser(client).then((user) => {
    console.log(user[0].userId);
    const userId = user[0].userId;
    getUserById(client, userId).then((u) => {
      console.log(u);
      const role = u[0].role;
      console.log(role);
      var admin = false;
      var seller = false;
      var buyer = false;
      if (role === "admin") {
        admin = true;
        seller = false;
        buyer = false;
      }
      if (role === "seller") {
        admin = false;
        seller = true;
        buyer = false;
      }
      if (role === "buyer") {
        admin = false;
        seller = false;
        buyer = true;
      }
      console.log("pass shops 1");
      getShops(client).then((shops) => {
        console.log(shops);
        res.render("shops", { shops, admin: admin, seller: seller, buyer: buyer, role: role });
      });
    });
  });
});

routerShops.get("/:id", async (req, res, next) => {});

routerShops.post("/", async (req, res, next) => {});

routerShops.put("/:id", async (req, res, next) => {});

routerShops.delete("/:id", async (req, res, next) => {});

module.exports = routerShops;
