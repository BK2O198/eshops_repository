class UserCredentials {
  constructor() {
    this.username = "";
    this.password = "";
    this.auth_token = "";
  }
}

module.exports = UserCredentials;
